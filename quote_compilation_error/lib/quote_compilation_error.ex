defmodule QuoteCompilationError do
  require MyMacro

  MyMacro.define_function_that_doesnt_compile()
end
