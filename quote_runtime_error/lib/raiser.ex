# Blank lines to help differentiate line numbers...











defmodule Raiser do
  defmacro define_function_that_raises do
    quote do # OK: 'lib/quote_runtime_error.ex:5'
    # quote location: :keep do # OK: 'lib/raiser.ex:26'
    # quote file: "ciao.ex" do # OK: 'ciao.ex:26'
    # quote line: 777 do # OK: 'lib/quote_runtime_error.ex:777'
      def function_that_raises do
        # Comment
        # Comment
        # Comment
        # Comment
        # Comment
        # Comment
        raise("I raise!")
      end
    end
  end
end
