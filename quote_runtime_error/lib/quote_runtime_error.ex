defmodule QuoteRuntimeError do
  require Raiser

  Raiser.define_function_that_raises()

  def run do
    function_that_raises()
  end
end
