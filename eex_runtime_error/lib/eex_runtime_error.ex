defmodule EExRuntimeError do
  require EExMacro

  EExMacro.define_eex_function_that_raises()

  def run do
    function_that_raises()
  end
end
