defmodule EExCompilationError do
  require EExMacro

  EExMacro.define_eex_function_that_doesnt_compile()
end
