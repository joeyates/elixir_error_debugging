# Blank lines to help differentiate line numbers...











defmodule MyMacro do
  defmacro define_function_that_doesnt_compile do
    # quote do # OK: lib/quote_compilation_error.ex:4
    quote location: :keep do # Wrong: lib/quote_compilation_error.ex:20
    # quote line: 999 do # OK: lib/quote_compilation_error.ex:999
    # quote file: "my_file.ex" do # Wrong: lib/quote_compilation_error.ex:20
      def function_that_deant_compile do
        some_undefined_function()
      end
    end
  end
end
