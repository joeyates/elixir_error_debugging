# Blank lines to help differentiate line numbers...











defmodule EExMacro do
  defmacro define_eex_function_that_doesnt_compile do
    # This EEx template will throw an error on line 3
    eex_source = """

      <%= call_an_undefined_function() %>
    """

    compiled = EEx.compile_string(eex_source)

    # quote do # File wrong: lib/eex_compilation_error.ex:5
    quote line: 999 do # File and line wrong: lib/eex_compilation_error.ex:2
    # quote file: "my_template.eex" do # File wrong: lib/eex_compilation_error.ex:5
    # quote location: :keep do # File wrong: lib/eex_compilation_error.ex:5
      def function_that_doesnt_compile do
        unquote(compiled)
      end
    end
  end
end
