defmodule EExMacro do
  defmacro define_eex_function_that_raises do
    eex_source = """
      \n\n\n

      <%= raise("I raise!") %>
    """
    compiled = EEx.compile_string(eex_source)

    quote do # OK
    # quote location: :keep do # OK
    # quote file: "my_template.eex" do # OK
      def function_that_raises do
        unquote(compiled)
      end
    end
  end
end
